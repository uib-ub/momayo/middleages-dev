# Fuseki docker

Basert på https://hub.docker.com/r/stain/jena-fuseki/

```
docker build . --tag fuseki 
``` 


```
docker run -p 3030:3030 --name fuseki --mount type=bind,source=$(pwd)/../out/,target=/staging/marcus/ --mount type=bind,source=$(pwd)/../ubbont-repository/ontology/,target=/staging/ontology/ fuseki
```

Gå inn i admin-grensesnitt og legg til nytt datasett med navn marcus-admin, velg persistent lagring.
Triplene er alt lastet inn i marcus-admin. De blir lastet inn fra ../out/marcus-bs.rdf-xml.owl og ../ubbont-repository/ontology/ubbont.owl

link må legges til i kitematic om en ønsker å bruke sammen med marcus.