# Middleages Marcus dokcer

The repository contains 4 Docker services, namely, Docker Fuseki, Docker Elasticsearch, Docker Blackbox and Docker WebApp . 

Containers need to be run in a composed environment to create a development environment same as that we have in production as of today.


## Architecture

![Alt text](docker-update-marcus.png?raw=true "Class diagrams")


## Note

- These containers share the same network. Therefore, they can communicate one another by calling their service names f.esk `http://fuseki:3030`.
- To run the containers, one must be in the directory where `docker-compose.yml` resides. 


## Build

To build the environment, run:

```bash
# Clone and install submodules
git clone git@git.app.uib.no:uib-ub/momayo/middleages-components.git
git submodule update --init

# Build images
docker-compose build

# Run dev setup
docker-compose up

# Stop dev setup
docker-compose down -v
```


## Running Docker fuseki

All other docker containers depends on Docker Fuseki. By default, fuseki uploads data and creates TDB that are then being copied to Elasticsearch Docker. Sometimes user might not want to load these data everytime when docker starts, this can now be achieved by setting the value of `"LOAD_FUSEKI_DATA_ON_START"` to empty in the file `.env`

## Data injection to Elasticsearch

To update data, you will have to run scripts  [elasticsearch-indexing-files](https://bitbucket.org/ubbdst/elasticsearch-indexing-files).
Note that, since now we are using Admin data, you will have to run the `"marcus-admin"` scripts. 

## Access

If all is well, Blackbox will be available at `localhost:8080/blackbox`, Elasticseach will be available at `localhost:9200` and Fuseki 
at `localhost:3030`

