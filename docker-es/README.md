# Elasticsearch

A [1.7.5](https://www.elastic.co/downloads/past-releases/elasticsearch-1-7-6) elasticsearch docker image. The image is supposed to be run in the 
composed environment with Docker Fuseki and Docker Blackbox. 


The image depends on Docker Fuseki, hence it must exist before running this one.

# Quick Start

To start a basic container, expose port 9200.

```
docker build . --tag es
docker run --volumes-from fuseki --name elasticsearch -p 9200:9200 -p 9300:9300 es
```

# Persistence

With the container, the volumes `/elasticsearch/data` can be persistent to the host machine.

 To start a default container with attached persistent/shared storage for data:

```sh
docker run --volumes-from fuseki --volume /es-data:/elasticsearch/data --name elasticsearch -p 9200:9200 -p 9300:9300 es
```

~